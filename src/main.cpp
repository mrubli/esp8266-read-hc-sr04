/*
 * HC-SR04 ultrasonic distance sensor
 *
 * Based on: https://github.com/sparkfun/HC-SR04_UltrasonicSensor
 *
 * NodeMCU pin assignment:
 * Vin: HC-SR04 Vcc
 * D4:  HC-SR04 Trig
 * D6:  HC-SR04 Echo
 *
 * Note that the HC-SR04 uses 5V signaling, so level shifting is required if GPIOs aren't able to
 * handle 5V. In practice a series resistor on the ECHO signal seems to work just fine but a
 * voltage divider is the nicer solution. Also see:
 * https://www.raspberrypi.org/forums/viewtopic.php?t=22841 (discussion on level shifting)
 * https://www.instructables.com/id/Simple-Arduino-and-HC-SR04-Example/ (uses 5V directly)
 */

#include <Arduino.h>


namespace
{

	constexpr auto TriggerGpio =  2;	// NodeMCU: D4
	constexpr auto EchoGpio    = 12;	// NodeMCU: D6

	// The speed of sound at 20 ℃ is about
	//   c = 343 [m/s] = 343 * 100 [cm] / 1'000'000 [µs] = 343 / 10'000 [cm/µs]
	// and the inverse is therefore:
	//   1/c = 10'000 / 343 [µs/cm] ≈ 29.155 [µs/cm]
	// The round trip takes twice as long, so our conversion factor is:
	//   2/c ≈ 58.309 [µs/cm]
	constexpr auto UsPerCm = 58.309f;	// Time-to-distance conversion factor [µs/cm]

	constexpr auto MaxDistance = 400 * UsPerCm;	// Maximum range

}


void setup()
{
	pinMode(TriggerGpio, OUTPUT);
	digitalWrite(TriggerGpio, LOW);

	Serial.begin(9600);
}

void loop()
{
	// Pull up the TRIGGER signal for 10 µs
	digitalWrite(TriggerGpio, HIGH);
	delayMicroseconds(10);
	digitalWrite(TriggerGpio, LOW);

	// Wait for the rising edge of the ECHO signal then measure how long the signal is high
	while (digitalRead(EchoGpio) == 0);
	const auto start = micros();
	while (digitalRead(EchoGpio) == 1);
	const auto end = micros();
	const auto pulseDurationUs = end - start;

	// Calculate the distance based on the duration
	const auto distanceCm = pulseDurationUs / UsPerCm;

	// Print out the results
	if(pulseDurationUs <= MaxDistance)
	{
		Serial.print(distanceCm);
		Serial.println(" cm");
	}
	else
	{
		Serial.println("Out of range");
	}

	// We need to wait at least 60 ms before the next measurement
	delay(1000);
}
